import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Personne from '../Personne/Personne'
import Page2 from '../Page2/Page2'
import Page3 from '../Page3/Page3'
import CardComponent from '../CardComponent/CardComponent'

import styled from 'styled-components'


const Nav = styled.nav`
    width: 100%;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
`
const UnderNav = styled.div`
    width: 100%;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    border: 1px solid silver
    padding: 25px;
    background-color: #00000012;
`

const LinkPage = styled(Link)`
&:hover{
    color: orange
}
    margin: 0 30px;
    color: black
    text-decoration: none;
`

const H1 = styled.span`
    font-size: 35px;
    color: rgba(0,0,0,0.9);
    text-transform: uppercase;
`

const DivTitle = styled.div`
background-image: url('https://www.cnewyork.net/wp-content/uploads/2016/03/panorama-new-york.jpg');
background-size: cover;
background-repeat: repeat;
background-position: center
width: 100%;
text-align: center;
padding: 35px;
`

const DivContainerFiche = styled.div`
width: 100%;
display: flex;
justify-content: space-around;
align-items: center;
flex-wrap: wrap;
`

class App extends Component{
    state = {
        hide: true
    }
    render(){
        return(
            <Router> 
                <div>
                    <Nav>
                        <DivTitle>
                            <LinkPage to="/">
                                <H1>Trombinoscope</H1>
                            </LinkPage>
                            <div>
                                <Route exact path="/"/>
                            </div>
                        </DivTitle>
                    </Nav>
                </div>

                <DivContainerFiche>
                    <CardComponent
                    prenom = {'Martin'}
                    nom = {'Hurel'}
                    imgUrl = {'https://dxsigner.com/wp-content/uploads/2018/10/creation-mascotte-avatar-entreprise-3D-animation-dxsigner.png'}
                    to={'./personne/1'}
                    />
                    <CardComponent 
                    prenom = {'Jean'}
                    nom = {'Charles'}
                    imgUrl = {'https://media.playmobil.com/i/playmobil/30111990_product_detail/Personnage%20fille?locale=fr-FR,fr,*&$pdp_product_main_xl$&strip=true&qlt=80&fmt.jpeg.chroma=1,1,1&unsharp=0,1,1,7&fmt.jpeg.interlaced=true'}
                    to={'./personne/2'}
                    />
                    <CardComponent 
                    prenom = {'Eric'}
                    nom = {'David'}
                    imgUrl = {'https://media.playmobil.com/i/playmobil/30009042_product_detail/Personnage%20homme?locale=fr-BE,fr,*&$pdp_product_main_xl$&strip=true&qlt=80&fmt.jpeg.chroma=1,1,1&unsharp=0,1,1,7&fmt.jpeg.interlaced=true'}
                    to={'./personne/3'}
                    />
                    <CardComponent 
                    prenom = {'Thomas'}
                    nom = {'Dutronc'}
                    imgUrl = {'http://fip-cleusbpub.com/6528-17156-thickbox/cle-usb-personnage-et-metier.jpg'}
                    to={'./personne/4'}
                    />
                    <CardComponent 
                    prenom = {'Jacques'}
                    nom = {'Andre'}
                    imgUrl = {'http://www.web-soluces.net/webmaster/avatar/AvatarMakerCom-Garcon.png'}
                    to={'./personne/5'}
                    />
                </DivContainerFiche>
                
                <div className="App">
                    <Route exact path="personne/:id" component={Personne}/>
                </div>  
            </Router>
        )
    }    
};

export default App;
