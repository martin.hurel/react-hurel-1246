import React, { Component } from 'react';
import styled from 'styled-components'

const FormNote = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center
`

const DivInput = styled.div`
    display: flex;
    width: 30%;
    align-items: center;
    justify-content: space-between;
    margin: 10px 0;
`
class Page2 extends Component {
    state = {
        linux: 4.4,
        node: 10.39,
        php: 12.22,
        react: 12,
        securite: 7.15,
        resultat: 9.59
    }

    getLinux = (e) => {
        let linux = ((6*1 + 4*4 + e.target.value * 4) / 9)
        linux = Math.round(linux*100)/100
        this.setState({linux})
        console.log('La '  + this.state.linux)
        this.handleSubmit()
    }

    getNode = (e) => {
        let node = ((15.25*1 + 9.18*4 + e.target.value * 4) / 9)
        node = Math.round(node*100)/100
        this.setState({node})
        this.handleSubmit()
    }

    getPhp = (e) => {
        let php = ((8*1 + 15.5*4 + 10*4 + e.target.value * 4) / 13)
        php = Math.round(php*100)/100
        this.setState({php})
        this.handleSubmit()
    }

    getReact = (e) => {
        let react = ((12*4 + e.target.value * 4) / 8)
        react = Math.round(react*100)/100
        this.setState({react})
        this.handleSubmit()
    }

    getSecurite = (e) => {
        let securite = ((13.1*1 + 9*1 + 5.20*4 + e.target.value * 4) / 10)
        securite = Math.round(securite*100)/100
        this.setState({securite})
        this.handleSubmit()
    }

    handleSubmit = () => {
        let resultat = ((30 + 12.13 + 13 + (this.state.securite) + (this.state.react * 2) + (this.state.php * 2) + this.state.node + this.state.linux * 3 ) / 14)
        console.log(this.state.linux)
        resultat = Math.round(resultat*100)/100
        this.setState({resultat})
    }
    render(){
        return(
            <div>
            <FormNote >
                <DivInput>
                <label>Linux : 4.4 => {this.state.linux}</label>
                <input type="number" onChange={this.getLinux}/>
                </DivInput>
                
                <DivInput>
                <label>Node : 10.39 => {this.state.node}</label>
                <input type="number" onChange={this.getNode}/>
                </DivInput>

                <DivInput>
                <label>Php : 12.22 => {this.state.php}</label>
                <input type="number" onChange={this.getPhp}/>
                </DivInput>

                <DivInput>
                <label>React : 12 => {this.state.react}</label>
                <input type="number" onChange={this.getReact}/>
                </DivInput>

                <DivInput>
                <label>Sécurité : 7.15 => {this.state.securite}</label>
                <input type="number" onChange={this.getSecurite}/>
                </DivInput>
            </FormNote>

            <p>Total : 9.59 => {this.state.resultat}</p>
            </div>
        )
    }
}

export default Page2