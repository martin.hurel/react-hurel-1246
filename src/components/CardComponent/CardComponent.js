import React, { Component } from 'react';
import styled from 'styled-components'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

const LinkPage = styled(Link)`
    width: 30%;
    height: 125;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: center;
    border: 2px solid silver;
    padding: 5px;
    margin: 15px 0;
`

const DivImage = styled.div`
    width: 100%;
    height: 150px;
    display: flex;
    background-size: cover;
    background-repeat: repeat;
    background-position: center
`

const SpanNom = styled.span`

`

const SpanPrenom = styled.span``
    
class CardComponent extends Component   {
render(){
    return(
<LinkPage to={this.props.to} >
    <DivImage style= {{backgroundImage: `url(${this.props.imgUrl})`}}></DivImage>
    <SpanNom>{this.props.prenom}</SpanNom>
    <SpanPrenom>{this.props.nom}</SpanPrenom>
</LinkPage>

    )
}
}

export default CardComponent